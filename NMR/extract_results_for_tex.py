#!/usr/bin/python
import os
import sys 
import re


####################################################
## 
##
##            Main
##
####################################################
if (len(sys.argv) < 2):
  print "Usage: %s results_file.blubb" % sys.argv[0]
  exit (1)

result_pattern = re.compile("\[1\] \"([A-Za-z_21]+) ([A_Za-z]+) MAE.RMS.COR\s+for\s+([A-Z]+)\s+:\s+([-0-9.]+)\s+([-0-9.]+)\s+([-0-9.]+)")
size_pattern   = re.compile("\[1\] \"size ([A_Za-z]+)\s+for ([A-Z]+) : ([0-9.]+)")
option_pattern = re.compile("\[1\] \" \*\* option ([A_Za-z_]+)\s+=\s+([A-Za-z0-9._]+)\"")
rf_option_pattern = re.compile("\[1\] \" \*\* option rf ([A_Za-z_]+)\s+=\s+([A-Za-z0-9._]+)\"")

pgm  = ""
atom = ""
mae  = 0.
rms  = 0.
cor  = 0.
sizes = "data size (train/test) &   "
models = {}
atoms  = {} 
atom_line  = "" 
options = {}

##
##   print the document header
##
doc_header = """\documentclass{article}
\usepackage[landscape]{geometry}
\\begin{document}"""

print doc_header 


##
##  first table: performance of the models per atom type
##
result_str = """\\begin{table}[htbp]
  \\begin{center}
   \\tiny
   \\begin{tabular}{%s}
   \hline
   program """

lines = open(sys.argv[1], 'r').readlines()
for line in lines:
  result        = re.match(result_pattern, line) 
  size_result   = re.match(size_pattern, line)
  option_result = re.match(option_pattern, line)
  rf_option_result = re.match(rf_option_pattern, line)
  if (result and len(result.groups())>4):
    if (result.group(2)=="test"):
#      print result.groups()
      pgm  = result.group(1)
      atom = result.group(3)
      mae  = result.group(4)
      rms  = result.group(5)
      cor  = result.group(6)
      if not atoms.has_key(atom):
        atom_line += " & " + atom
        atoms[atom] = 1
      if models.has_key(pgm):
        models[pgm] = models[pgm] + " & " + str(cor) + " (" + str(rms) + ") "
      else:
        #print "*********", line, pgm, atom
        models[pgm] = pgm + " & " + str(cor) + " (" + str(rms) + ") "

  # do we have a new size line?
  if (size_result and len(size_result.groups())>2):
    #print size_result.groups()
    size = size_result.group(3)
    atom = size_result.group(2)
    train = False
    if (size_result.group(1)=="train"):
      train = True
      sizes += size + " / "
    else:
      sizes += size + " & "
  # the options:  
  if (option_result and len(option_result.groups())>1):
    opt = option_result.group(1).replace("_", "\_")
    val = option_result.group(2).replace("_", "\_")
    #print "###", opt, val
    options[opt] = val
  if (rf_option_result and len(rf_option_result.groups())>1):
    opt = rf_option_result.group(1).replace("_", "\_")
    val = rf_option_result.group(2).replace("_", "\_")
    #print "###", opt, val
    options[opt] = val

result_str += atom_line + "  \\\\\n\hline\n\hline\n" 
for m in models:
  result_str += models[m] + "\\\\\n"
result_str += "\n\hline\n"  
result_str += sizes[:-2]+"\\\\\n"

result_str += """\hline
\end{tabular}
\end{center}
\caption{Evaluation of the model. The values given are correlation and RMS per atom type.}
\end{table}"""
#\end{document}"""

##
##   print the first table
##
result_str = result_str % ("|l|" + ("c|" * len(atoms)))
print result_str

###
## now the parameter/options table:
###
option_result_str = """
\\begin{table}[htbp]
  \\begin{center}
   \\tiny
   \\begin{tabular}{|l|c|}
   \hline
   option & value   \\\\\n\hline\n\hline\n"""

for opt, val in options.iteritems():
  #print opt, val
  option_result_str += opt + "  &  " + val + " \\\\\n"

option_result_str += """\hline \n
\end{tabular}
\end{center}
\caption{Options and parameters of the current model.}
\end{table}"""

print option_result_str


###
## now the document footer
###
print """\end{document}"""

