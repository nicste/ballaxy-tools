#!/bin/bash

# remember the directory from which we were called
olddir=$(pwd)

# change into our pipeline base directory
cd $(dirname $0)

source $(dirname $0)/NMR_pipeline_definitions.sh

python $(dirname $0)/storeHomologyInformation.py $@

# go back
cd ${olddir}

