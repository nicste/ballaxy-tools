#!/bin/bash

##TODO: rename to computeNMRDescriptorDB.sh (see its galaxy.xml file)

# remember the directory from which we were called
olddir=$(pwd)

# change into our pipeline base directory
cd $(dirname $0)

source $(dirname $0)/NMR_pipeline_definitions.sh

export PATH=${source_dir}/../build:${PATH}

###################################################

# explain how to use the script
NO_ARGS=0 
E_OPTERROR=85
if [ $# -eq "$NO_ARGS" ]    # Script invoked with no command-line args?
then
  echo "Usage: `basename $0` options (--mapping=... --features=... --outfile=... --predtype=[PureProtein|ProteinLigand] )" 
  exit $E_OPTERROR          # Exit and explain usage.
                            # Usage: scriptname -options
                            # Note: dash (-) necessary
fi

# parse the command line arguments
while getopts ":-:" Option; do
  case $Option in
		- )
			case "${OPTARG}" in
				mapping=*)
					export mapping_file=${OPTARG#*=}
				  ;;
				features=*)
					export feature_file=${OPTARG#*=}
				  ;;
				outfile=*)
					export model_db=${OPTARG#*=}	
          ;;
        predtype=*)
					export prediction_type=${OPTARG#*=}
				  ;;
			esac
  esac
done

# check command line arguments
if [ "${mapping_file}" = "" ]; then
  echo "Error: invalid mapping file!"
  exit $E_OPTERROR  
fi

if [ "${feature_file}" = "" ]; then
  echo "Error: invalid feature file!"
  exit $E_OPTERROR  
fi

if [ "${model_db}" = "" ]; then
  echo "Error: invalid model db file!"
  exit $E_OPTERROR  
fi

if [ "${prediction_type}" = "" ]; then
  echo "Error: invalid prediction type!"
  exit $E_OPTERROR  
fi

if [ "${prediction_type}" = "PureProtein" ]; then
	echo "  Pure Protein case"
elif [ "${prediction_type}" = "ProteinLigand" ]; then
	echo "The Protein - Ligand option is currently not yet implemented. Sorry!"
	exit
else	
	echo "Unknown prediction type ", ${prediction_type}
	exit $E_OPTERROR
fi

nmr_sqlite_db=${model_db}
mappingfilename=${mapping_file}
#TODO: logs per user $$

debug=false
if ${debug}; then	
	# for debugging
	echo "---------------------------------------"
	echo "        fillNMRPredictDB.sh            "
	echo "---------------------------------------"
	echo "nmr_sqlite_db: ${nmr_sqlite_db}"
	echo "mappingfilename: ${mappingfilename}"
	echo "prediction_type: ${prediction_type}" 
	echo "num_filldb_procs: ${num_filldb_procs}"
	echo "bmrbhome: ${bmrbhome}"
	echo "pdbhome: ${pdbhome}"
	echo "logdir:  ${logdir}"
	echo "---------------------------------------"
fi

############################################

echo "**************************************************************************"
echo "  Create PDB to BMRB table..."
echo "      (see ${logdir}create_pdb_bmrb_table.log)"
echo "      (find DB in ${nmr_sqlite_db})"
echo "**************************************************************************"	
sqlite3 ${nmr_sqlite_db} "drop table if exists PDB_BMRB;"
python createPDB_BMRBTable.py --DB=${nmr_sqlite_db} \
                             	--mappingfile=${mappingfilename} \
														  > ${logdir}create_pdb_bmrb_table.log 2>&1

#cp /home/HPL/anne/DEVELOP/GALAXY/NMR/testdb.sqlite ${nmr_sqlite_db}
echo -n "      Number of mappings added:" 
sqlite3 ${nmr_sqlite_db} "select count(distinct PDB_ID) from PDB_BMRB;"

echo "**************************************************************************"
echo "  Create PDB atom to BMRB atom table... (this may take a while...)"
echo "      (see ${logdir}sql/\*)" 
echo "      (find DB in ${nmr_sqlite_db})"
echo "**************************************************************************"
sqlite3 ${nmr_sqlite_db} "drop table if exists ATOM_PROPS;"
./fillNMRDBParallel.sh --db=${nmr_sqlite_db} \
											 --pred_type=${prediction_type} \
											 --num_proc=${num_filldb_procs} \
											 --feature_file=${feature_file} \
											 --mapping_file=${mappingfilename} \
											 --bmrb_home=${bmrbhome} \
											 --pdb_home=${pdbhome} 

echo -n "      Number of mappings with computed features:" 
sqlite3 ${nmr_sqlite_db} "select count(distinct PDB_ID) from ATOM_PROPS;"

# go back
cd ${olddir}

echo " Done."
# done.
