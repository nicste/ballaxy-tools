#!/usr/bin/python

#
#		NightShift: NMR shift inference by General Hybrid model Training 
#		 --- A pipeline for automated NMR shift prediction training ---
#		
#		Anna Dehof
#

import sys, os
import sqlite3
import urllib2, urllib, xml.dom.minidom
import re
import BALL
import pprint

from nmr_definitions import *

url = 'http://www.rcsb.org/pdb/rest/'

clustlwhome = ""


pdb_bmrb_columns =  "PDB_ID char(4), PDB_CHAIN_ID char(1), PDB_CHAIN_INDEX int, BMRB_ID char(5), BMRB_ChemUnit varchar(256), ALIGN_SCORE real, Alignment_PDB varchar(512), Alignment_BMRB varchar(512), PDB_R_FACT real default -1.0, PDB_YEAR int default -1.0,  NMR_YEAR int default -1.0, PROTEIN_SIZE int default -1, HAS_H_SHIFTS bool default 0, HAS_C_SHIFTS bool default 0, HAS_N_SHIFTS bool default 0, IS_NMR bool default true, NMR_SPECT char(20) default 'unknown', NMR_SPECT_MANUFAC char(20) default 'unknown', NMR_FIELD_STRENGTH real default -1, HAS_ION int default 0, HAS_LIGAND int default 0, HAS_DNA int default 0, LIGAND_SIZE int default -1, IONS char(30)"


#
#####
class Alignment:
  score = -42.
  pdb_chain = ""
  pdb_chain_index = -1
  bmrb_component = ""
  pdb_alignment = ""
  bmrb_alignment = ""

  def __str__(self):
    result  = "Score: %f Chain: %s(%d) Component: %s\n" % (self.score, self.pdb_chain, self.pdb_chain_index, self.bmrb_component)
    result +=  self.pdb_alignment+"\n"
    result += self.bmrb_alignment+"\n"

    return result


# compute the size of a protein
#####
def getSizeOf(alignment):
  val = 0
  for a in alignment:
    if (a != "-"):
      val=val+1
  return val


# compute the alignment and its score for a given pdb/chain bmrb/component pair
#####
def computeAlignment(pdb_chain_sequence, bmrb_component_sequence):
  fasta_file = open(datadir+"fasta", 'w')
  fasta_file.write(">pdb\n"+pdb_chain_sequence+"\n")
  fasta_file.write(">bmrb\n"+bmrb_component_sequence+"\n")
  fasta_file.close()

  #print pdb_chain_sequence, bmrb_component_sequence
  # call clustalw
  os.system("clustalw -INFILE=%s/fasta -OUTFILE=%s/alignment > %s/alignment.log" % (datadir, datadir, datadir));

  # read the alignment
  pdb_align  = ""
  bmrb_align = ""
  align_lines = open(datadir+"/alignment", 'r').readlines()
  for a in align_lines:
    if "pdb" in a:
      pdb_align  += a.split()[1].strip()
    elif "bmrb" in a:
      bmrb_align += a.split()[1].strip()

  # and the score
  align_lines = open(datadir+"/alignment.log", 'r').readlines()
  score = -42.
  for a in align_lines:
    if "Aligned. Score:" in a:
      score = float(a.split()[-1])
  if score == -42.:
    print "    WARNING: No alignment score found"

  a = Alignment()
  a.score = score
  a.pdb_alignment = pdb_align
  a.bmrb_alignment = bmrb_align

  return a

#
#####
def computeBestAlignment(pdb_id, current_bmrb_file):
  # get NMR seq via BALL
  nmr_file = BALL.NMRStarFile(current_bmrb_file, BALL.OpenMode(BALL.File.MODE_IN))

  # iterate over all monomeric polymers
  components = []
  for c in range(nmr_file.getNumberOfMonomericPolymers()):
    poly = nmr_file.getMonomericPolymer(c)
    components.append((str(poly.label_name), str(poly.residue_sequence)))

  # and over all chains

  # find the best alignment_scored chain of the pdb file
  #   NOTE: in our special case this is not necessary 
  # get PDB seq via pdb.org 
  filehandle = urllib.urlopen('http://www.rcsb.org/pdb/files/fasta.txt?structureIdList='+pdb_id)
  pdb_fasta = "" 
  pdb_lines = filehandle.readlines()
  filehandle.close()
  text =  ''.join(pdb_lines[1:])

  chains = []

  current_chain_id = ""
  current_chain_index = -1
  current_sequence = ""
  for i in range(len(pdb_lines)):
    l = pdb_lines[i]
    if ">" in l:
      if (i != 0):
        chains.append((current_chain_id, current_chain_index, current_sequence))
      current_chain_id = l.split("|")[0][-1]
      current_chain_index += 1
      current_sequence = ""
    else:
      current_sequence += pdb_lines[i].strip()
  chains.append((current_chain_id, current_chain_index, current_sequence))

  result = []

  for chain in chains:
    best_result = Alignment()
    best_result.score = -42.
    best_result.pdb_chain = chain[0]
    best_result.pdb_chain_index = chain[1]

    for comp in components:
      chain_result = computeAlignment(chain[2], comp[1])
      if (chain_result.score > best_result.score):
        best_result.bmrb_component = comp[0]
        best_result.pdb_alignment = chain_result.pdb_alignment
        best_result.bmrb_alignment = chain_result.bmrb_alignment
        best_result.score = chain_result.score
    result.append(best_result)

  nmr_file.close()
  return result

####################################################
##
##
##              MAIN
##
##
####################################################
if (len(sys.argv) < 2):
  print "Usage batchmode: %s  --mappingfile=...  --DB=..."  % sys.argv[0]
  #<pdb-bmrb-mapping-file> [-DB <full_path_to_DB>]" % sys.argv[0]
  print "Usage explicit : %s --pdb=... --bmrb=... --DB=..."  % sys.argv[0]
  #<pdb_id>  <bmrb_id>" % sys.argv[0]
  exit (1)
  
  #python ../NMR/createPDB_BMRBTable.py --pdb=/local/anne/NMR/pipeline/nmr-data/PDB_PROTEIN/1A5J.pdb --bmrb=/local/anne/NMR/pipeline/nmr-data/BMRB_PROTEIN/5517.str --DB=Spinster.sqlite


# parse the command line arguments
command_args = {}
for arg in sys.argv[1:]:
  sp = arg.split("=")
  command_args[sp[0][2:]] = sp[1]

import pprint
pprint.pprint(command_args)

### DB
if not command_args.has_key("DB"):
  print("Error: No database given!")
  exit (1)

nmr_sqlite_db = command_args["DB"]

### mappingfile
# are we in batch mode or explicit mode?
batch_mode = False
if command_args.has_key("mappingfile"):
  batch_mode = True
elif (command_args.has_key("pdb") and command_args.has_key("bmrb")):
  batch_mode = False
else:
  print("Error: please chose between explicit or batchmode!")
  exit (1)


print "Connect to database ", nmr_sqlite_db, " ...\n"
# connect to our database
conn = sqlite3.connect(nmr_sqlite_db)

# create a cursor
c = conn.cursor()

# Create the table PDB_BMRB
c.execute("pragma table_info(PDB_BMRB)")
if c.fetchall() == []:
  c.execute("create table PDB_BMRB( " + pdb_bmrb_columns + ")")
  print "   created table PDB_BMRB\n"
else:
  print "   table PDB_BMRB already existed\n"

# read the mapping
print "Read the mapping(s)..."
mapping = {}
mapping_lines = []
if batch_mode:
  mappingfile = open(command_args["mappingfile"], 'r') #sys.argv[1], 'r')
  mapping_lines = mappingfile.readlines()
  print "      from file ", mappingfile
else:
  pdb_id  = command_args["pdb"]#sys.argv[1]
  bmrb_id = command_args["bmrb"]#sys.argv[2]
  mapping_lines.append(pdb_id + " " + bmrb_id + "\n")
  print "     from pair ", pdb_id, " ", bmrb_id
 
print "Check each mapping... (total number:", len(mapping_lines),")"
# create an alignment for each mapping
for line in mapping_lines:#[:15]:
  v = line.strip().split()
  pdb_id  = v[0]
  bmrb_id = v[1]
  current_bmrb_file = bmrb_id
  chain_id = ""
  #print "current pair: ", pdb_id, bmrb_id,
  
  # did we get a full path?
  if (not os.path.exists(current_bmrb_file)):
    current_bmrb_file = bmrbhome + bmrb_id + ".str"

  #print "\n---", pdb_id, bmrb_id, current_bmrb_file 

  ##############################################
  ## 
  ##       get the mapped pair related data 
  ##
  ##############################################

  # for future PDB-BMRB NMR data 
  is_nmr = True 

  # for future PDB-BMRB NMR data 
  pdb_r_factor = -1

  # for future PDB-BMRB NMR data 
  pdb_year = -1

  # get the NMR data
  print current_bmrb_file
  nmr_file           = BALL.NMRStarFile(current_bmrb_file, BALL.OpenMode(BALL.File.MODE_IN))
  nmr_spec           = nmr_file.getNMRSpectrometer(0).model
  nmr_spec_manufac   = nmr_file.getNMRSpectrometerManufacturer(0)
  nmr_field_strength = nmr_file.getNMRSpectrometerFieldStrength(0)
  #print "   nmr hardware: ", nmr_spec, "---", nmr_spec_manufac, "---", nmr_field_strength 
  # 
  nmr_year = -1

  # call BALL to read pH etc
  # TODO: get the path to readNMR_PH correct
  os.system("readNMR_PH " + current_bmrb_file + " > NMR_experiment_values");
  experiment_lines = open("NMR_experiment_values", 'r').readlines()
  
  #
  has_dna  = 0

  # parse the pH etc
  for exp_line in experiment_lines:
    exp_va = exp_line.strip().split()
    if (len(exp_va) > 1):
      if exp_va[0] == "Submission_year:":
        nmr_year = exp_va[1].split("-")[0] #  2006-11-02
#     if exp_va[0] == "NMR_solution:":
#       nmr_solution = exp_va[1]
#     if exp_va[0] == "NMR_exp_ph:":
#       nmr_ph = exp_va[1]
#     if exp_va[0] == "NMR_exp_press:":
#       nmr_pressure = exp_va[1]
#      if exp_va[0] == "NMR_exp_temp:":
#        nmr_temp = exp_va[1]
      if exp_va[0] == "Monomeric_polymer_classes:":
        for poly in exp_va[1:]:
          if poly == "DNA":
            has_dna += 1

  # print "   nmr year ", nmr_year
  # print "   ph/temp/press/sol :", nmr_ph, nmr_temp,  nmr_pressure , nmr_solution

  # shift types
  has_H_shifts = nmr_file.hasHshifts()
  has_C_shifts = nmr_file.hasCshifts()
  has_N_shifts = nmr_file.hasNshifts()

  # clean up
  nmr_file.close()

  # 
  has_ion = 0
  has_ligand = 0
  #has_dna = 0 # is already computed by the readNMR_PH script
  
  # ask pdb for ligand and ion information
  req = urllib2.Request(url+'ligandInfo?structureId='+pdb_id)
  #print url+'ligandInfo?structureId='+pdb_id
  try:
    xmldata=urllib2.urlopen(req).read()
    document = xml.dom.minidom.parseString(xmldata)
  except Exception, e:
    print "Query PDB for " + pdb_id + " failed due to reason:"
    print e


  #print xmldata
  has_ion = False
  has_complex_ligand = False
  ion_list = []
  ligand_sizes = []
  ligands = []
  # check each ligand
  for ligand in document.getElementsByTagName("ligand"):
    chemName = ""
    chemFormula = "" 
    atom_counter = 0
    for name in ligand.getElementsByTagName("chemicalName"):
      chemName =  name.childNodes[0].nodeValue.strip()+";"
    for formula in ligand.getElementsByTagName("formula"):
      chemFormula =  formula.childNodes[0].nodeValue.strip()
    
    #print "+", chemFormula
    if (" ION;" in chemName):
      atms = chemFormula.split()
      for a in atms:
        if a.isdigit():
          print "   Ion ", a, " has charge: ", atms[-1]
        else:
          ion_list.append(a)
          has_ion = True
    else:
      has_complex_ligand = True
      atms = chemFormula.split()
      for a in atms:
        #print a
        m = re.match('([a-zA-Z]+)([0-9]*)', a)
        if m and len(m.group(2))>0:
          #print "#atoms:", (m.group(2))
          atom_counter += (int)(m.group(2))
      ligand_sizes.append(atom_counter)
      ligands.append(chemFormula)
      #print chemFormula, atom_counter

  if (len(ligand_sizes) > 1):
    print "   WARNING: More than 1 ligand!", ligands, ligand_sizes 
  
  #print ion_list 
  #ions         IONS char(30) 
  has_ion = len(ion_list)
  has_ligand = len(ligands)
  # we are interested in the overall number of ligand atoms
  ligand_size = 0
  for lig in ligand_sizes:
    ligand_size += lig
  
  #print  pdb_id, bmrb_id,  nmr_year, #'''pdb_r_factor, pdb_year,'''
  #print  has_H_shifts, has_C_shifts, has_N_shifts, is_nmr, nmr_spec, nmr_spec_manufac, nmr_field_strength, #nmr_solution,
  #print  has_ion, has_ligand, has_dna, ligand_size, ion_list

  # the alignment score
  alignment = computeBestAlignment(pdb_id, current_bmrb_file) #bmrb_id)
 
 
  for a in alignment: 
    # check if same chain 
    if (chain_id != ""):
      if (chain_id != a.pdb_chain):
        continue

    #print chain_id, a.pdb_chain, a.score
    bmrb_chem_unit = a.bmrb_component
    pdb_chain_id = a.pdb_chain
    pdb_chain_index = a.pdb_chain_index
    align_score = a.score
    align_pdb = a.pdb_alignment
    align_bmrb = a.bmrb_alignment 
    prot_size = getSizeOf(a.pdb_alignment)
    if batch_mode:
      # store in db  
      try:
        c.execute("insert into PDB_BMRB (PDB_ID, PDB_CHAIN_ID, PDB_CHAIN_INDEX, BMRB_ID, BMRB_ChemUnit, ALIGN_SCORE, Alignment_PDB, Alignment_BMRB, PDB_R_FACT, PDB_YEAR, NMR_YEAR, PROTEIN_SIZE, HAS_H_SHIFTS, HAS_C_SHIFTS, HAS_N_SHIFTS, IS_NMR, NMR_SPECT, NMR_SPECT_MANUFAC, NMR_FIELD_STRENGTH, HAS_ION, HAS_LIGAND, HAS_DNA, LIGAND_SIZE, IONS) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", 
                                    (pdb_id, pdb_chain_id, pdb_chain_index, bmrb_id, bmrb_chem_unit, align_score, align_pdb, align_bmrb, pdb_r_factor, pdb_year, nmr_year, prot_size, has_H_shifts, has_C_shifts, has_N_shifts, is_nmr, str(nmr_spec), str(nmr_spec_manufac), nmr_field_strength, has_ion, has_ligand, has_dna, ligand_size, str(ion_list) ))
        conn.commit()
      except sqlite3.Error, e:
        print "An sqlite error:", e.args[0]
        print "sqlite error for ", pdb_id, pdb_chain_id, pdb_chain_index, bmrb_id
    else:
      print  pdb_id, pdb_chain_id, pdb_chain_index, bmrb_id, bmrb_chem_unit, align_score, align_pdb, align_bmrb, pdb_r_factor, pdb_year, nmr_year, prot_size, has_H_shifts, has_C_shifts, has_N_shifts, is_nmr, str(nmr_spec), str(nmr_spec_manufac), nmr_field_strength, has_ion, has_ligand, has_dna, ligand_size, str(ion_list)

# close mapping
if batch_mode:
  mappingfile.close()

  # Save (commit) the changes
  conn.commit()

  # close the cursor if we are done with it
  c.close()

print "Done."
